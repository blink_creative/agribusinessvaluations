<?php
/**
 * Template Name: Homepage
 *
 * A custom homepage
 *
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<!--//Splash-->

	<div id="pager-container">
		<div id="pager"></div>
	</div>
	<div id="frame-splash">
		<div class="slide-one">
			<div class="wrapper">
				<div class="slide">
					<div class="slide-details">
						<h1>Certified Practising Valuers</h1>
						<p>Valuations of agribusiness assets for all purposes...</p>
						<p><a href="http://www.agribusinessvaluations.com.au/about/">&gt; more</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="slide-two">
			<div class="wrapper">
				<div class="slide">
					<div class="slide-details">
						<h1>Agricultural Economic Analysts</h1>
						<p>Dedicated in-house agricultural economic analysis services...</p>
						<p><a href="http://www.agribusinessvaluations.com.au/services/">&gt; more</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="slide-three">
			<div class="wrapper">
				<div class="slide">
					<div class="slide-details">
						<h1>Compensation Consultants</h1>
						<p>Valuations for compulsory acquisition of rural property...</p>
						<p><a href="http://www.agribusinessvaluations.com.au/industries/compulsory-farmland-acquisition/">&gt; more</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

<!--//End Splash-->

	<div id="frame-content">
		<div id="watermark-container">
			<div class="wrapper">
				<div class="intro-text"><?php the_field('intro_text'); ?></div>
		
				<!--//Content-->
				
				<div id="three-column">
					<ul>
						<li id="navigation-module">
							<ul>
								<li class="first"><a class="rounded" href="/industries/viticulture-wine/">Viticulture &amp; Wine</a></li>
								<li><a href="/industries/pastoral-horticulture/">Pastoral &amp; Horticulture</a></li>
								<li><a href="/industries/plant-machinery/">Plant &amp; Machinery</a></li>
								<li><a href="/industries/forestry/">Forestry</a></li>
								<li class="last"><a class="rounded" href="/industries/poultry/">Poultry</a></li>
							</ul>
						</li>
						<li class="module" onclick="window.location.href='/services/'">
							<div id="services-module">
								<h3>Services</h3>
								<div class="module-copy">
									<p>Sam Paton has been providing agribusiness valuation services...</p>
									<p><a href="/services/">> more</a></p>
								</div>
							</div>
						</li>
						<li class="module" onclick="window.location.href='/news/'">
							<div id="news-module">
								<h3>News</h3>
								<div class="module-copy">
									<p>Keep in touch with the latest news from Agribusiness Valuations Australia...</p>
									<p><a href="/news/">> more</a></p>
								</div>
							</div>
						</li>
					</ul>
					<div class="clear"></div>
				</div>

				<!--//End Content-->
						
				<div class="clear"></div>
				<div id="credentials">
					<div id="logo-api" class="float-left"><a href="http://www.api.org.au/" target="_blank">API Logo</a></div>
					<p class="float-left">A Fellow of the Australian Property Institute and the Royal Institute of Chartered Surveyors (RICS), <br/>Sam Paton is a member of the Agricultural and Resource Economics Society of Australia.</p>
					<div class="clear"></div>
				</div>
			</div>
			<div id="watermark"><img src="<?php bloginfo('template_url'); ?>/images/background-watermark.jpg" alt="Watermark"></div>
		</div>
	</div>

<?php get_footer(); ?>