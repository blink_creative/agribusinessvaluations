<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<!--//Splash-->

	<div id="frame-splash-subpage"><img src="<?php bloginfo('template_url'); ?>/images/Header_image_News.jpg"/></div>

<!--//End Splash-->


<!--//Content-->

	<div id="frame-content">
		<div id="watermark-container">
			<div class="wrapper">
				<div class="intro-text"><p><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></p></div>
				<div id="two-column">
					<div id="left-column" class="float-left">
						<div id="blog-list">
							<?php
							/* Run the loop for the search to output the results.
							 * If you want to overload this in a child theme then include a file
							 * called loop-search.php and that will be used instead.
							 */
							 get_template_part( 'loop', 'search' );
							?>
						</div>
					</div>
					<div id="right-column" class="float-right">
						<ul id="sidebar">
							<li class="module" onclick="window.location.href='/services/'">
								<div id="services-module">
									<h3>Services</h3>
									<div class="module-copy">
										<p>Sam Paton has been providing agribusiness valuation services...</p>
										<p><a href="/services/">> more</a></p>
									</div>
								</div>
							</li>
							<li class="module" onclick="window.location.href='/industries/'">
								<div id="expertise-module">
									<h3>Expertise</h3>
									<div class="module-copy">
										<p>With a large national client base, Sam Paton has provided an independent...</p>
										<p><a href="/industries/">> more</a></p>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div id="watermark"><img src="<?php bloginfo('template_url'); ?>/images/background-watermark.jpg" alt="Watermark"></div>
		</div>
	</div>
	
<!--//End Content-->


<?php get_footer(); ?>
