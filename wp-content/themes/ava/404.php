<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<!--//Content-->

	<div id="frame-content">
		<div id="watermark-container">
			<div class="wrapper">
				<div id="two-column" class="blog-post">
					<div id="left-column" class="float-left">
						<h1>Page not found!</h1>
						<p>Please use the navigation above to find the area you are looking for.</p>
					</div>
					<div id="right-column" class="float-right">
						<ul id="sidebar">
							<li class="module" onclick="window.location.href='/clients/'">
								<div id="clients-module">
									<h3>Clients</h3>
									<div class="module-copy">
										<p>AVA have practised throughout Australasia on specialist...</p>
										<p><a href="/clients/">> more</a></p>
									</div>
								</div>
							</li>
							<li class="module" onclick="window.location.href='/news/'">
								<div id="news-module">
									<h3>News</h3>
									<div class="module-copy">
										<p>Keep in touch with the latest news from Agribusiness Valuations Australia...</p>
										<p><a href="/news/">> more</a></p>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div id="watermark"><img src="<?php bloginfo('template_url'); ?>/images/background-watermark.jpg" alt="Watermark"></div>
		</div>
	</div>
	
<!--//End Content-->

<?php get_footer(); ?>