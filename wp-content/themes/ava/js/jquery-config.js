// JavaScript Document

$(document).ready(function() {

	/*--Slider--*/

	$('#frame-splash').cycle({
		pager: '#pager',
		timeout: 5000
	});
	
	/*--Rounded--*/
	
	$(function() {
    if (window.PIE) {
        $('.rounded').each(function() {
            PIE.attach(this);
        });
		}
	});
	
	$(function() {
    if (window.PIE) {
        $('.module').each(function() {
            PIE.attach(this);
        });
		}
	});
	
});