<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<!--//Splash-->

	<div id="frame-splash-subpage"><img src="<?php bloginfo('template_url'); ?>/images/Header_image_News.jpg"/></div>
	

<!--//End Splash-->

<!--//Content-->

	<div id="frame-content">
		<div id="watermark-container">
			<div class="wrapper">
				<div id="two-column" class="blog-post">
					<div id="left-column" class="float-left">
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<h1><?php the_title(); ?></h1>
							<p>Posted On: <?php the_date(); ?></p>
							<?php the_content(); ?>
							<p><a href="javascript:history.go(-1)" onMouseOver="self.status=document.referrer;return true">Back to news</a></p>
						<?php endwhile; // end of the loop. ?>
					</div>
					<div id="right-column" class="float-right">
						<ul id="sidebar">
							<li class="module" onclick="window.location.href='/clients/'">
								<div id="clients-module">
									<h3>Clients</h3>
									<div class="module-copy">
										<p>AVA have practised throughout Australasia on specialist...</p>
										<p><a href="/clients/">> more</a></p>
									</div>
								</div>
							</li>
							<li class="module" onclick="window.location.href='/news/'">
								<div id="news-module">
									<h3>News</h3>
									<div class="module-copy">
										<p>Keep in touch with the latest news from Agribusiness Valuations Australia...</p>
										<p><a href="/news/">> more</a></p>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div id="watermark"><img src="<?php bloginfo('template_url'); ?>/images/background-watermark.jpg" alt="Watermark"></div>
		</div>
	</div>
	
<!--//End Content-->

<?php get_footer(); ?>