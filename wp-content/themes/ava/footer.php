<!--//Footer-->

	<div id="frame-footer">
		<div class="wrapper">
			<h3 class="float-left">Agribusiness Valuations Australia</h3>
			<div id="links" class="float-right">
				<ul class="social float-left">
					<li class="email-us"><a href="mailto:sam@agrivaluations.com.au">Email us today</a></li>
					<li class="network-linkedin"><a target="_blank" href="http://au.linkedin.com/pub/sam-paton/41/508/280">Join us on LinkedIn</a></li>
				</ul>
				<div id="logo-ava" class="float-right"><a href="/">Agribusiness Valuations Australia Logo</a></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div id="sitemap">
				<ul>
					<li class="footer-about"><a href="/about/">About</a>
						<p><a href="/about/">Sam Paton Qualified Agribusiness Valuation<br/>Specialist</a></p>
					</li>
					<li class="footer-services"><a href="/services/">Services</a>
						<p><a href="/about/">Agribusiness Valuations and Economic Analysis</a></p>
					</li>
					<li class="footer-clients"><a href="/clients/">Clients</a>
						<ul>
							<li><a href="/clients/">Accountants</a></li>
							<li><a href="/clients/">Banks</a></li>
							<li><a href="/clients/">Barristers/Courts</a></li>
							<li><a href="/clients/">Corporations</a></li>
							<li><a href="/clients/">Government</a></li>
							<li><a href="/clients/">Insurance Companies</a></li>
							<li><a href="/clients/">Private Investors</a></li>
							<li><a href="/clients/">Solicitors Courts</a></li>
						</ul>
					</li>
					<li class="footer-industry"><a href="#">Expertise</a>
						<ul>
							<?php wp_list_pages('title_li=&child_of=11&sort_column=menu_order'); ?>
						</ul>
					</li>
					<li class="footer-news"><a href="#">News</a>
						<ul>
							<?php
							$args = array( 'numberposts' => 3, 'order'=> 'ASC', 'orderby' => 'title' );
							$postslist = get_posts( $args );
							foreach ($postslist as $post) :  setup_postdata($post); ?> 
								<li>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>   
								</li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="footer-contact"><a href="#">Contact</a>
						<ul>
							<li>
								<p>PO Box 206,
								<br/>Mitcham VIC 3132
								<br/><strong>T</strong> (03) 9873 1122
								<br/><strong>F</strong> (03) 8802 4378
								<br/><strong>M</strong> 0409 348 937
								<br/><strong>E</strong> <a href="mailto:sam@agrivaluations.com.au">sam@agrivaluations.com.au</a></p>
							</li>
							<li class="blink">
								<p><a href="http://www.blinkcreative.com.au" target="_blank">Site by Blink</a></p>
							</li>
						</ul>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

<!--//End Footer-->


<!--//Scripts-->

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/jquery-core.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-slider.js"></script>
	
<!--[if IE]>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/PIE.js"></script>
<![endif]-->	
	
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-config.js"></script>

<!--//End Scripts-->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46307107-3', 'auto');
  ga('send', 'pageview');

</script>


<!-- Wordpress Scripts -->

	<?php wp_footer(); ?>
	
<!-- End Wordpress Scripts -->

</body>
</html>
