<?php
/**
 * Template Name: Contact
 *
 * A custom homepage
 *
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<!--//Splash-->

	<div id="frame-splash-subpage"><?php the_post_thumbnail('splash'); ?></div>

<!--//End Splash-->

<!--//Content-->

	<div id="frame-content">
		<div id="watermark-container">
			<div class="wrapper">
				<div class="intro-text"><?php the_field('intro_text'); ?></div>
				<div id="two-column">
					<div id="left-column" class="float-left">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
					<div id="right-column" class="float-right">
						<ul id="sidebar">
							<li class="module" onclick="window.location.href='/services/'">
								<div id="services-module">
									<h3>Services</h3>
									<div class="module-copy">
										<p>Sam Paton has been providing agribusiness valuation services...</p>
										<p><a href="/services/">> more</a></p>
									</div>
								</div>
							</li>
							<li class="module" onclick="window.location.href='/news/'">
								<div id="news-module">
									<h3>News</h3>
										
									<div class="module-copy">
										<p>Keep in touch with the latest news from Agribusiness Valuations Australia...</p>
										<p><a href="/news/">> more</a></p>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div id="watermark"><img src="<?php bloginfo('template_url'); ?>/images/background-watermark.jpg" alt="Watermark"></div>
		</div>
	</div>
	
<!--//End Content-->

<?php endwhile; ?>

<?php get_footer(); ?>