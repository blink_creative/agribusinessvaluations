<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Meta -->

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>

<!-- End Meta -->

<!-- Stylesheets -->

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/layout.css" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
<!-- End Stylesheets -->

<!-- Wordpress Scripts -->

	<?php wp_head(); ?>

<!-- End Wordpress Scripts -->

</head>

<body <?php body_class(); ?>>

<!--//Header-->
	
	<div id="frame-header">
		<div class="wrapper">
			<div id="logo-main" class="float-left">
				<a href="/" title="Click here to return to the main page."><img src="<?php bloginfo('template_url'); ?>/images/logo.gif" alt="Agribusiness Valuations Australia" /></a>
			</div>
			<div id="networks" class="float-right">
				<ul id="social" class="float-right">
					<li class="email-us"><a href="mailto:sam@agrivaluations.com.au">Email us today</a></li>
					<li class="network-linkedin"><a target="_blank" href="http://au.linkedin.com/pub/sam-paton/41/508/280">Join us on LinkedIn</a></li>
				</ul>
				<div class="float-right search-form">
					<form role="search" method="get" action="<?php echo site_url(); ?>">
						<fieldset>
							<div class="float-left">
								<input type="text" onblur="if (value=='') value='Search'" onfocus="if (value =='Search') value=''" value="Search" name="s" id="s" />
							</div>
							<div class="clear"></div>
						</fieldset>
					</form>
				</div>
				<div id="navigation-main" class="clear">
					<?php $menuproperties = array(
						'container' => 'ul'
					);
					?>
					<?php wp_nav_menu($menuproperties); ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

<!--//End Header-->
